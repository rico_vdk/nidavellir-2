/**
* Provides everything necessary for the 3D viewer on the home page of Nidavellir.
* From rendering the scene and models, to making sure the aspect ratio is kept on window resize.
* 
* Project: 				Nidavellir
* Authored by (date): 	Rico van der Klein (20-11-2020)
* Updated by (date):	
*/

import * as THREE from './three/three.module.js';
import { STLLoader } from './three/STLLoader.js';
import { OrbitControls } from './three/OrbitControls.js';

const viewer	= document.getElementById('viewer-3d');
const container = document.getElementById('3d-canvas');
const scene 	= new THREE.Scene();
const cam 		= new THREE.PerspectiveCamera(50, container.clientWidth / container.clientHeight, 1, 1000);
const renderer	= new THREE.WebGLRenderer({ antialias : true });
const stlLoader	= new STLLoader();
const canvas 	= renderer.domElement;
const controls 	= new OrbitControls(cam, canvas);
const light 	= new THREE.AmbientLight(0x404040);

let renderedFileName 	= '';
let tanFOV 				= 0;

/**
* Initialises the 3D model viewer so that it may be ready to render any model.
*
* @return {void}
*/
function setup() {
	const hlight 			= new THREE.AmbientLight (0x404040, .5);
	const directionalLight 	= new THREE.DirectionalLight(0xffffff, .5);
	const light 			= new THREE.PointLight(0xc4c4c4, .5);
	const light2 			= new THREE.PointLight(0xc4c4c4, .5);
	const light3 			= new THREE.PointLight(0xc4c4c4, .5);
	const light4 			= new THREE.PointLight(0xc4c4c4, .5);

	scene.background = new THREE.Color(0xdddddd);

	/* cam.rotation.y = 45/180*Math.PI; */
	cam.position.x = 16;
	cam.position.y = 8;
	cam.position.z = 16;
	cam.lookAt(scene.position);

	tanFOV = Math.tan(((Math.PI / 180) * cam.fov / 2 ));

	directionalLight.position.set(0,1,0);
	directionalLight.castShadow = true;
	
	light.position.set(0, 300, 500);
	light2.position.set(500, 100, 0);
	light3.position.set(0, 100, -500);
	light4.position.set(-500, 300, 500);

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.shadowMap.enabled 	= true;
	renderer.outputEncoding 	= THREE.sRGBEncoding;
	
	scene.add(hlight);
	scene.add(directionalLight);
	scene.add(light);
	scene.add(light2);
	scene.add(light3);
	scene.add(light4);
}

/**
* Loads a file by path followed by the given file name.
* Lastly, the file is rendered into 3D view.
*
* @param {string} fileName The name of the file to load.
* @return {void}
*/
function load(fileName) {
	stlLoader.load(
		`../../storage/uploads/${fileName}`,

		(geometry) => {
			const material 	= new THREE.MeshStandardMaterial();
			const mesh 		= new THREE.Mesh(geometry);

			mesh.material = material;
			mesh.name = fileName;

			if (renderedFileName != '') {
				removeSceneModels();
			}

			scene.add(mesh);
			render();

			renderedFileName = fileName;
		},

		undefined,

		(error) => {
			console.log(error)
		}
	);
}

/**
* Tells the render engine to render the current scene every frame.
*
* @return {void}
*/
function render() {
	renderer.render(scene, cam);
	requestAnimationFrame(render);
}

/**
* Updates the canvas aspect ratio based on the container's width and height.
*
* @return {void}
*/
function updateCanvasAspect() {
	cam.aspect 	= window.innerWidth / window.innerHeight;
	cam.fov 	= (360 / Math.PI) * Math.atan(tanFOV * (window.innerHeight / window.innerHeight)); // No idea but it works.

	cam.lookAt(scene.position);
	cam.updateProjectionMatrix();
}

/**
* Gets rid of all the meshes active in the current scene.
*
* @return {void}
*/
function removeSceneModels() {
	scene.children.forEach((child, i) => {
		if (child instanceof THREE.Mesh)
			scene.remove(child);
	})
}

container.appendChild(canvas);
canvas.classList.add('keep-aspect');

window.addEventListener('fileprinting', () => {
	if (currentFileName != renderedFileName && currentFileName != '') {
		load(currentFileName);
	}
});

window.addEventListener('resize', () => {
	updateCanvasAspect();
});

setup();
render();
updateCanvasAspect();