/**
* Administrator functionalities for uploads & users overview.
* Allows for updating and removing entries.
* 
* Project: 				Nidavellir
* Authored by (date): 	Rico van der Klein (26-11-2020)
* Updated by (date):	
*/

(($) => {
	const saveButtons 			= Array.from($('.save-button'));	// Updates row when pressed.
	const deleteButtons 		= Array.from($('.delete-button'));	// Removes row when pressed.
	const csrfToken 			= $('input[name ="_token"]').val();	// Laravel data validation.
	const confirmTimeout 		= 1;								// Amount of seconds you have to confirm row deletion.
	const updatingButtons 		= [];								// Debounce to prevent requests from firing multiple times.
	const confirmableButtons	= [];								// Delete buttons that can be pressed again to confirm the deletion.
	const routes 				= {									// List of valid routes.
		updateUpload	: 'uploads/update',
		updateUser 		: 'users/update',
		deleteUpload 	: 'uploads/delete',
		deleteUser 		: 'users/delete',
	};

	/**
	 * Setup event listeners.
	 * 
	 * @return void
	 */
	const init = () => {
		saveButtons.forEach((button) => {
			$(button).on({
				click: () => { onSaveButtonClicked(button) }
			});
		});

		deleteButtons.forEach((button) => {
			$(button).on({
				click: () => { onDeleteButtonClicked(button) }
			});
		});
	}

	/**
	 * Iterate over confirmable buttons and see if they are timed out.
	 * 
	 * @return void
	 */
	const checkAndUpdateConfirmableButtons = () => {
		const now = Date.now();

		confirmableButtons.forEach((confirmable) => {
			const elapsed = (now - confirmable.timed) / 1000;

			if (elapsed > confirmTimeout) {
				$(confirmable.button).removeClass('confirm-delete');
				$(confirmable.button).text('X');
				
				confirmableButtons.splice(confirmableButtons.indexOf(confirmable), 1);
			}
		});
	}

	/**
	 * Returns a preset object with data to be used with every ajax call.
	 * 
	 * @param {object} tr Table row object.
	 * @param {HTMLElement} button Button element.
	 * @param {string} route Request url.
	 * @param {object} data The data to process on the server.
	 * 
	 * @return {object} Ajax object data.
	 */
	const createAjaxObject = (tr, button, route, data) => {
		data._method 	= 'PUT';
		data._token 	= csrfToken;

		return { // Woah we actually return something!!
			url 		: route,
			type 		: 'POST',
			data		: JSON.stringify(data),
			contentType : 'application/json; charset=utf-8',
			success 	: (data) => { onRowSuccess(tr, button, data) },
			error 		: (error) => { onRowError(tr, button, error) }
		}
	}

	/**
	 * Callback method.
	 * Adds debounce and initiates a save.
	 * 
	 * @param {HTMLElement} button Button element.
	 * 
	 * @return void
	 */
	const onSaveButtonClicked = (button) => {
		if (!updatingButtons.includes(button)) {
			const tr = $(button).parent()

			updatingButtons.push(button);

			saveRow(tr, button);
		}
	}

	/**
	 * Callback method.
	 * Adds debounce and initiates a delete.
	 * 
	 * @param {HTMLElement} button Button element.
	 * 
	 * @return void
	 */
	const onDeleteButtonClicked = (button) => {
		const buttonConfirmable = confirmableButtons.find(confirmable => button == confirmable.button);

		if (!updatingButtons.includes(button) && buttonConfirmable != undefined) {
			const tr = $(button).parent()

			updatingButtons.push(button);

			deleteRow(tr, button);
		} else if (buttonConfirmable == undefined) {
			confirmableButtons.push({
				button 	: button,
				timed 	: Date.now(),
			});

			$(button).addClass('confirm-delete');
			$(button).text('O');
		}
	}

	/**
	 * Sets up data to be sent to the server for updating.
	 * 
	 * @param {object} tr Table row element.
	 * @param {HTMLElement} button Button element.
	 * 
	 * @return void
	 */
	const saveRow = (tr, button) => {
		const routeType = $(button).attr('route-type');
		const route 	= routes[routeType];
		const data 		= {};
		
		data.id 		= tr.attr('col-id');
		data.routeType 	= routeType;

		switch (routeType) {
			case 'updateUpload':
				data.status 	= tr.find('select[class ="status-select"] option:checked').val();
				data.comment 	= tr.find('input[class ="comment-input"]').val();
				data.queuePos	= tr.find('input[class ="queue-pos-input"]').val();
			break;

			case 'updateUser':
				data.status = tr.find('select[class ="status-select"] option:checked').val();
				data.rank 	= tr.find('select[class ="rank-select"] option:checked').val();
			break;
		}

		$.ajax(createAjaxObject(tr, button, route, data));
	}

	/**
	 * Sets up data to be sent to the server for deletion.
	 * Adds debounce and initiates a delete.
	 * 
	 * @param {object} tr Table row element.
	 * @param {HTMLElement} button Button element.
	 * 
	 * @return void
	 */
	const deleteRow = (tr, button) => {
		const routeType = $(button).attr('route-type');
		const route 	= routes[routeType];
		const data 		= { id : tr.attr('col-id'), routeType : routeType };

		$.ajax(createAjaxObject(tr, button, route, data));
	}

	/**
	 * Callback method for when an ajax call was successful.
	 * Applies styling.
	 * 
	 * @param {object} tr Table row element.
	 * @param {HTMLElement} button Button element.
	 * @param {object} data Server response.
	 * 
	 * @return void
	 */
	const onRowSuccess = (tr, button, data) => {
		console.log(data);

		if (data.hasOwnProperty('error')) {
			onRowError(tr, button, data.error);

			return;
		}

		switch (data.routeType) {
			case 'updateUpload': case 'updateUser':
				if (!tr.hasClass('success'))
					tr.addClass('success');

				if (tr.hasClass('error'))
					tr.removeClass('error');
			break;

			case 'deleteUpload': case 'deleteUser':
				const typeTextDisplay = data.routeType == 'deleteUpload' ? 'Bestand' : 'Gebruiker'; // Turn this into another switch case if you have a third data set.

				tr.html(`<td style="text-align: center">${typeTextDisplay} verwijderd.</td>`);
				tr.addClass('removed-entry');
			break;
		}

		updatingButtons.splice(updatingButtons.indexOf(button), 1);
	}

	/**
	 * Callback method for when an ajax call was unsuccessful.
	 * Applies styling.
	 * 
	 * @param {object} tr Table row element.
	 * @param {HTMLElement} button Button element.
	 * @param {object} error Server response.
	 * 
	 * @return void
	 */
	const onRowError = (tr, button, error) => {
		console.log(error);

		if (!tr.hasClass('error'))
			tr.addClass('error');

		if (tr.hasClass('success'))
			tr.removeClass('success');

		updatingButtons.splice(updatingButtons.indexOf(button), 1);
	}

	init();
	setInterval(() => { checkAndUpdateConfirmableButtons() }, 0250);
})(jQuery)