/**
* Provides app functionality for special tables.
* 
* Project: 				Nidavellir
* Authored by (date): 	Rico van der Klein (8-10-2020)
* Updated by (date):	
*/

const filterHeads 		= document.getElementsByClassName('filter-head'); // All table headers.
const filterBody		= document.getElementsByClassName('filter-body')[0] ?? null; // Element containing all table rows.
const ajaxSelects		= filterBody.getElementsByClassName('ajax-select'); // All inputs of type select we should handle by Ajax.
const ajaxButtons 		= filterBody.getElementsByClassName('ajax-button'); // All buttons we should handle by Ajax.
const ajaxTexts 		= filterBody.getElementsByClassName('ajax-input-text'); // All text fields we should handle by Ajax, if that wasn't obvious enough.
const csrf 				= document.getElementsByName('_token')[0] ?? null; // Verification for laravel.
const urls 				= { user : '/users/update', upload : '/uploads/update' }; // Object containing paths for updating entries through Ajax.
const classes			= { updating : 'updating', success : 'success', error : 'error', confirmDelete : 'confirm-delete' }; // Various classes for styling purposes to convey a status to the user.
const groups 			= {}; // Object with all table group information stored in it.
const deletePresses 	= []; // List of buttons that were pressed with the stored date. They are automatically removed if the time exceeds deleteTimer, allowing them to be pressed.
const updating 			= []; // List of select inputs that are currently updating through Ajax. Used to see if the input is already being processed.
const deleteTimer 		= 1; // Time window the user has to confirm the deletion.

let currentGroup = {};

/**
 * ---------- FUNCTIONS ----------
 */

/**
 * Filters the table based on stored group information.
 * 
 * @param {object} group The group to filter the table on.
 */
function filterGroup(group) {
	const trs 		= [];
	const label 	= group.th.getElementsByClassName('filter-button')[0];
	const removed 	= filterBody.getElementsByClassName('removed-entry');

	// Reset the current group's filter.
	if (group != currentGroup) {
		const currentLabel = currentGroup.th.getElementsByClassName('filter-button')[0];

		currentGroup.currentFilter 		= 1;
		groups[currentGroup.groupName] 	= currentGroup;

		// Also update the small icon on the th's label.
		currentLabel.textContent = '-';
	}

	// Remove all wiped entries before sorting.
	Array.from(removed).forEach((tr) => {
		tr.remove();
	});
	
	// Start filtering the currently clicked group.
	group.currentFilter = group.currentFilter == 0 ? 1 : 0;

	group.tds.sort((a, b) => {
		const inputA 	= a.getElementsByTagName('input')[0];
		const inputB 	= a.getElementsByTagName('input')[0];
		const selectA 	= a.getElementsByTagName('select')[0];
		const selectB 	= a.getElementsByTagName('select')[0];
		let contentA 	= a.textContent;
		let contentB 	= b.textContent;

		if (inputA)
			contentA = inputA.value;

			if (contentA.length < 1)
				contentA = inputA.placeholder;
		else if (selectA)
			contentA = selectA.options[selectA.selectedIndex].textContent;
		
		if (inputB)
			contentB = inputB.value;

			if (contentB.length < 1)
				contentB = inputB.placeholder;
		else if (selectB)
			contentB = selectB.options[selectB.selectedIndex].textContent;

		contentA = contentA.replace(/\s/g, '');
		contentB = contentB.replace(/\s/g, '');
		
		return contentA.localeCompare(contentB);
	});

	if (group.currentFilter == 1) {
		group.tds.reverse();

		label.textContent = '^';
	} else {
		label.textContent = 'v';
	}
	
	groups[group.groupName] = group;
	currentGroup = group;

	// Update table visuals.
	group.tds.forEach((td, i) => {
		const tr = td.parentElement;

		if (!trs.includes(tr))
			trs.push(tr);
	});

	trs.forEach((tr, i) => {
		if (tr)
			filterBody.appendChild(tr);
	});
}

/**
 * Returns the appropriate url based on the given type.
 * The urls can be defined in a constant at the top of the script.
 * 
 * @param {string} type References the constant variables indicating the path of said type.
 * @returns {string} The appropriate url based on given type.
 */
function getUrlFromType(type) {
	return urls.hasOwnProperty(type) ? urls[type] : '';
}

/**
 * Swap the input class safely.
 * 
 * @param {object} select The input to have it's class changed.
 * @param {string} newClass The new class to assign to the input.
 */
function changeClass(object, newClass) {
	for (let classEntry in classes) {
		if (classes.hasOwnProperty(classEntry)) {
			if (object.classList.contains(classEntry))
				object.classList.remove(classEntry);
		}
	}
	
	object.classList.add(newClass);
}

/**
 * ---------- ON STARTUP ----------
 */

// Loop over all table headers and get it's information, then store it in a group.
Array.from(filterHeads).forEach((th, i) => {
	const group = th.getAttribute('group');
	const tds 	= Array.from(filterBody.querySelectorAll(`td[group=${group}]`));

	if (!groups.hasOwnProperty(group)) {
		groups[group] = {
			groupName 		: group,
			currentFilter 	: 1,		// <0 : 'desc', 1 : 'asc'>, set to 1 in order to filter by desc by default and vice versa.
			th 				: th,
			tds 			: tds,		// Store an easily sortable array of tds.
		};
	}

	if (i == 0)
		currentGroup = groups[group];

	th.onclick = () => { filterGroup(groups[group]) }
});

// Initiate the first filter on page visit.
filterGroup(currentGroup);