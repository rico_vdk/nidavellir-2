/**
* Provides view switching functionality for the home page.
* 
* Project: 				Nidavellir
* Authored by (date): 	Rico van der Klein (8-10-2020)
* Updated by (date):	
*/

const switches = document.getElementsByClassName('view-switch');
const toggleClass = 'shown';
const groups = {};

/**
* Depending on the button being pressed, toggles an element's visibility.
*
* @param {object} textField The input field to get data from.
* @return {void}
*/
function switchView(viewSwitch) {
	const group = viewSwitch.getAttribute('group');
	const toggle = viewSwitch.getAttribute('toggle');
	const toggleElement = document.getElementById(toggle);
	const activeElement = groups[group].activeElement;

	if (!viewSwitch.classList.contains('selected')) {
		viewSwitch.classList.add('selected');

		groups[group].activeSwitch.classList.remove('selected');
		
		groups[group].activeSwitch = viewSwitch;
	}

	if (toggleElement != activeElement) {
		groups[group].activeElement = toggleElement

		if (activeElement.classList.contains(toggleClass))
			activeElement.classList.remove(toggleClass);
		
		toggleElement.classList.add(toggleClass);
	}
}

Array.from(switches).forEach(element => {
	const group = element.getAttribute('group');
	const toggle = element.getAttribute('toggle');
	const toggleElement = document.getElementById(toggle);

	if (!groups.hasOwnProperty(group)) {
		groups[group] = {
			switches : [element],
			activeElement: element.classList.contains('selected') ? toggleElement : null,
			activeSwitch: element.classList.contains('selected') ? element : null,
		};
	} else {
		groups[group]['switches'].push(element);
		
		if (element.classList.contains('selected')) {
			groups[group].activeElement = toggleElement;
			groups[group].activeSwitch = element;
		}
	}

	element.onclick = () => { switchView(element) };
});

switchView(switches[0]);