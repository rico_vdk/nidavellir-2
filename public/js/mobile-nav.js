const nav 		= document.getElementById('nav');
const toggle 	= document.getElementById('nav-toggle');
const icon 		= toggle.getElementsByTagName('i')[0];

const init = () => {
	toggle.addEventListener('click', onNavToggleClick);
}

const onNavToggleClick = () => {
	if (!nav.classList.contains('open')) {
		nav.classList.add('open');

		icon.classList.remove('fa-angle-double-right');
		icon.classList.add('fa-angle-double-left');
	} else {
		nav.classList.remove('open');

		icon.classList.remove('fa-angle-double-left');
		icon.classList.add('fa-angle-double-right');
	}
}

init();