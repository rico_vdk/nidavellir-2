/**
* Retrieves chart diagnostics and print jobs from the printer every five seconds.
* Additionally, a live queue is updated and can show the currently printing file.
* 
* Project: 				Nidavellir
* Authored by (date): 	Rico van der Klein (20-11-2020)
* Updated by (date):	
*/

const timeFormat 			= 'mm:ss';
const temperatureFlowUrl	= '/api/printer/diagnostics/temperature_flow/1'; // Diagnostics.
const printJobUrl			= '/api/print_job'; // Current print job.
const printJobsUrl			= '/api/print_jobs'; // History (unused).
const queueUrl 				= '/queue'; // File queue.
const chartContext 			= $('#temperature-chart')[0].getContext("2d");
const queueBody 			= document.getElementById('queue-entries').getElementsByTagName('tbody')[0];
const queueMsgTr 			= queueBody.getElementsByTagName('tr')[0];
const queueMsgTd			= queueBody.getElementsByTagName('td')[0];
const homeTab 				= document.getElementById('home-tab');
const stream				= document.getElementById('stream');
const bedGradient 			= chartContext.createLinearGradient(0, 0, 0, 300);
const extruderOneGradient 	= chartContext.createLinearGradient(0, 0, 0, 200);
const extruderTwoGradient 	= chartContext.createLinearGradient(0, 0, 0, 200);
const filePrintingEvent 	= new Event('fileprinting');
const chartOptions 			= {
	type: 'line',
	data: {
		labels: [],
		datasets: [],
	},
	options: {
		responsive: true,
		maintainAspectRatio: false,
		scales: {
			xAxes: [{
				type: 'time',
				time: {
					tooltipFormat: 'HH:mm:ss',
					unit: 'second',
					unitStepSize: 5,
					displayFormats: {
						'second': 'HH:mm:ss'
					}
				},
				scaleLabel: {
					display: false,
				},
				ticks: {
					display: false,
				},
				gridLines: {
					display: false,
				},
			}],
			yAxes: [{
				stacked: false,
				ticks: {
					beginAtZero: true,
					callback: function(value, index, values) {
						return value + ' °C';
					}
				},
				scaleLabel: {
					display: false,
				},
				gridLines: {
					display: false,
				},
			}],
		},
		tooltips: {
			intersect: false,
			mode: 'nearest',
			/*callbacks: {
				label: function(tooltipItem, myData) {
					var label = myData.datasets[tooltipItem.datasetIndex].label || '';
					if (label) {
						label += ': ';
					}
					label += parseFloat(tooltipItem.value).toFixed(2);
					return label;
				}
			}*/
		},
		legend: false,
	}
};

let bedTemperature 		= [];
let queueRows 			= [];
let temperatureChart 	= null;
let currentFileName 	= '';

/**
* Gets rid of all rows in the queue.
*
* @return {void}
*/
function RemoveQueueRows() {
	queueRows.forEach((row) => {
		queueBody.removeChild(row);
	});

	queueRows = [];
}

/**
* Retrieves a list of files in the queue and displays them to the user.
*
* @return {void}
*/
function UpdateQueueEntries() {
	$.ajax({
		method: 'GET',
		url: 	queueUrl,

		success : function(response) {
			if (response.hasOwnProperty('queue')) {
				if (response.queue.length) {
					queueMsgTr.style['display'] = 'none';

					RemoveQueueRows();

					response.queue.forEach((entry, i) => {
						const status 	= currentFileName == entry.name ? 'In de oven' : 'Wachten..';
						const pos 		= i++ < 10 ? '0' + i++ : i++;

						let tr = document.createElement('tr');

						tr.classList.add('entry');

						tr.innerHTML = `
							<td class="small-col">${ pos }</td>
							<td>${ entry.real_name }</td>
							<td>${ status }</td>
						`;

						queueRows.push(tr);
						queueBody.appendChild(tr);
					});
				} else {
					RemoveQueueRows();

					queueMsgTd.textContent = 'Er staat niets in de wachtrij.';
					queueMsgTr.style['display'] = 'block';
				}
			}
		},

		error : function(response) {
			RemoveQueueRows();

			queueMsgTr.style['display'] = 'block';
			queueMsgTd.textContent = 'Er is iets fout gegaan in Nidavellir.. :(';
		}
	});
}

/**
* Retrieves temperature data regarding the 3D printer and inputs this into the chart for the user to view.
*
* @return {void}
*/
function UpdateTemperatureFlow() {
	$.ajax({
		method: 'GET',
		url: 	temperatureFlowUrl,

		success: function (response) {
			temperatureChart.data.labels.push(Date.now());
			temperatureChart.data.labels.shift();

			temperatureChart.data.datasets[0].data.push(response[1][11].toFixed(2));
			temperatureChart.data.datasets[0].data.shift();

			temperatureChart.data.datasets[1].data.push(response[1][12].toFixed(2));
			temperatureChart.data.datasets[1].data.shift();

			temperatureChart.data.datasets[2].data.push(response[1][1].toFixed(2));
			temperatureChart.data.datasets[2].data.shift();

			temperatureChart.data.datasets[3].data.push(response[1][2].toFixed(2));
			temperatureChart.data.datasets[3].data.shift();

			temperatureChart.data.datasets[4].data.push(response[1][6].toFixed(2));
			temperatureChart.data.datasets[4].data.shift();

			temperatureChart.data.datasets[5].data.push(response[1][7].toFixed(2));
			temperatureChart.data.datasets[5].data.shift();

			temperatureChart.update();
		},

		error: function (response) {
			console.log(response);
		}
	});
}

/**
* Retrieves data regarding the currently printing file and displays this to the user.
*
* @return {void}
*/
function UpdatePrintJob() {
	$.ajax({
		method: 'GET',
		url: 	printJobUrl,

		success: function (response) {
			for (let key in response) {
				let detail = $('[data-detail="' + key + '"]');

				console.log(key);
				
				if (response.hasOwnProperty(key) && detail.length && response[key]) {
					let data = response[key];

					if (key == 'name') {
						currentFileName = data;

						window.dispatchEvent(filePrintingEvent);
					}

					if (key === 'state') {
						data = (data.charAt(0).toUpperCase() + data.slice(1)).replace('_', ' ');
					}

					if (key === 'time_elapsed' || key === 'time_total') {
						data = moment().startOf('day').seconds(data).format('HH:mm:ss');
					}

					if (key === 'progress') {
						data = (data * 100).toFixed(2) + '%';
					}

					if (key === 'datetime_started' || key === 'datetime_finished') {
						data = moment(data).add(2, 'hours').format('DD-MM-YYYY HH:mm:ss');
					}

					detail.html(data);
				}
			}
		},

		error: function (jqXHR, textStatus, errorThrown) {
			console.log('Er is geen taak gevonden waarmee de printer nu bezig is.');
		}
	});
}

for (let i = 12; i >= 0; i--) {
	chartOptions.data.labels.push(Date.now() - ((1000 * i) * 5));
}

bedGradient.addColorStop(0, 'rgba(233, 30, 99, .1)');
bedGradient.addColorStop(1, 'rgba(233, 30, 99, 0)');

extruderOneGradient.addColorStop(0, 'rgba(0, 188, 212, .1)');
extruderOneGradient.addColorStop(1, 'rgba(0, 188, 212, 0)');

extruderTwoGradient.addColorStop(0, 'rgba(255, 152, 0, .1)');
extruderTwoGradient.addColorStop(1, 'rgba(255, 152, 0, 0)');

chartOptions.data.datasets = [
	{
		label: 'Bed',
		borderColor: 'rgba(233, 30, 99, 1)',
		backgroundColor: bedGradient,
		data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		pointRadius: 0,
		pointHoverRadius: 0,
		lineTension: 0,
	},
	{
		label: 'Bed Target',
		borderColor: 'rgba(233, 30, 99, 1)',
		data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		borderDash: [10, 5],
		fill: false,
		pointRadius: 0,
		pointHoverRadius: 0,
		lineTension: 0,
	},
	{
		label: 'Extruder 1',
		borderColor: 'rgba(255, 152, 0, 1)',
		backgroundColor: extruderTwoGradient,
		data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		pointRadius: 0,
		pointHoverRadius: 0,
		lineTension: 0,
	},
	{
		label: 'Extruder 1 Target',
		borderColor: 'rgba(255, 152, 0, 1)',
		data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		borderDash: [10, 5],
		fill: false,
		pointRadius: 0,
		pointHoverRadius: 0,
		lineTension: 0,
	},
	{
		label: 'Extruder 2',
		borderColor: 'rgba(0, 188, 212, 1)',
		backgroundColor: extruderOneGradient,
		data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		pointRadius: 0,
		pointHoverRadius: 0,
		lineTension: 0,
	},
	{
		label: 'Extruder 2 Target',
		borderColor: 'rgba(0, 188, 212, 1)',
		data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		borderDash: [10, 5],
		fill: false,
		pointRadius: 0,
		pointHoverRadius: 0,
		lineTension: 0,
	},
];

temperatureChart = new Chart($('#temperature-chart'), chartOptions);

setInterval(function () {
	UpdateTemperatureFlow();
	UpdatePrintJob();
	UpdateQueueEntries();
}, 5000);

// Funnies.
stream.addEventListener('error', () => {
	homeTab.textContent = 'Vijver';
})

/* $.ajax({ // History
	method: 'GET',
	url: printJobsUrl,
	success: function (response) {
		for (let job in response) {
			$('.table tbody').append(
				'<tr>' +
				'<td>' + response[job].result + '</td>' +
				'<td>' + response[job].name + '</td>' +
				'<td>' + moment(response[job].datetime_started).add(2, 'hours').format('DD-MM-YYYY HH:mm:ss') + '</td>' +
				'<td>' + moment(response[job].datetime_finished).add(2, 'hours').format('DD-MM-YYYY HH:mm:ss') + '</td>' +
				'<td>' + moment(response[job].datetime_cleaned).add(2, 'hours').format('DD-MM-YYYY HH:mm:ss') + '</td>' +
				'</tr>'
			)
		}
	},
	error: function (a, b) {
		console.log(a);
	},
}); */