<!DOCTYPE html>
<html lang="nl">
	<head>
		@include('includes.head')
	</head>
	<body>
		<div id="nav" class="material-shadow">
			@include('includes.nav')
		</div>

		<div id="content">
			@yield('content')
		</div>

		<script type="text/javascript" src="{{ asset('js/mobile-nav.js') }}"></script>
	</body>
</html>