@extends('layouts.page')

@section('content')
	<div class="container-grid full-content">

		<!-- Livestream/3D Viewer -->
		<div id="viewer" class="dash-entry material-shadow">

			<!-- Livestream -->
			<div id="viewer-stream" class="viewer-feed">
				<div class="viewer-canvas">
					<object id="stream" class="keep-aspect embed-responsive-item" data="http://195.169.7.101:8080/?action=stream" type="image/png">
						<img src="https://etc.usf.edu/clippix/pix/two-ducks-in-reflecting-pond_medium.jpg" style="-webkit-user-select: none; margin: auto; width: 100%; height: 100%">
					</object>
					
				</div>
			</div>

			<!-- 3D Viewer -->
			<div id="viewer-3d" class="viewer-feed shown">
				<div class="viewer-canvas" id="3d-canvas">
				</div>
			</div>

			<!-- View switcher -->
			<button class="view-switch" group="viewer" toggle="viewer-stream">
				Livestream
			</button>
			
			<button class="view-switch selected" group="viewer" toggle="viewer-3d">
				3D Weergave
			</button>
		</div>

		<!-- Printer & Queue info -->
		<div id="info" class="dash-entry material-shadow">

			<!-- Printer info -->
			<div id="info-statistics" class="info-feed shown">
				@php
					// Contains all the info entries you would like to render in the loop below.
					$infoEntries = [
						['title' => 'Status', 				'data_detail' => 'state'],
						['title' => 'Model', 				'data_detail' => 'name'],
						['title' => 'Progressie', 			'data_detail' => 'progress'],
						['title' => 'Tijd bezig', 			'data_detail' => 'time_elapsed'],
						['title' => 'Geschatte tijdsduur', 	'data_detail' => 'time_total'],
						['title' => 'Tijd gestart', 		'data_detail' => 'datetime_started'],
						['title' => 'Tijd geëindigd', 		'data_detail' => 'datetime_finished'],
					];
				@endphp

				<!-- Info list -->
				<table class="info-entries">
					@foreach ($infoEntries as $infoEntry)
						<tr class="entry">
							<th class="entry-title">{{ $infoEntry['title'] }}</th>
							<td class="entry-status" data-detail="{{ $infoEntry['data_detail'] }}">Geen data beschikbaar</td>
						</tr>
					@endforeach
				</table>

				<div class="info-graph">
					<canvas id="temperature-chart"></canvas>
				</div>
			</div>

			<!-- Queue info -->
			<div id="info-queue" class="info-feed table-container-vert">
				<table id="queue-entries" class="queue-entries table-scroll">
					<thead>
						<tr>
							<th class="small-col"><label>Positie</label></th>
							<th><label>Bestandsnaam</label></th>
							<th><label>Status</label></th>
						</tr>
					</thead>
					<tbody>
						<tr id="placeholder-queue-entry" class="entry">
							<td style="text-align: center; font-style: italic;">Bezig met ophalen van de wachtrij..</td>
						</tr>
						{{-- @if (isset($queue) && count($queue) > 0)
							@foreach ($queue as $entry)
								<tr class="entry">
									<td class="small-col">{{ sprintf('%02d', $loop->index + 1) }}</td>
									<td>{{ $entry->name }}</td>
									<td>Nee</td>
								</tr>
							@endforeach
						@else
							<tr class="entry">
								<td style="text-align: center; font-style: italic;">De wachtrij is leeg.</td>
							</tr>
						@endif --}}
					</tbody>
				</table>
			</div>

			<!-- Info switcher -->
			<button class="view-switch selected" group="info" toggle="info-statistics">
				Oven Info
			</button>

			<button class="view-switch" group="info" toggle="info-queue">
				Wachtrij
			</button>
		</div>

		<meta name="base_url" content="{{ URL::to('/') }}">
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/view-switch.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/printer.js') }}"></script>
		<script type="module" src="{{ asset('js/3d.js') }}"></script>
	</div>
@endsection