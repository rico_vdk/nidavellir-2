@extends('layouts.page')

@section('content')
	<div class="container-grid full-content">
		<div class="dash-auth dash-entry padded material-shadow">
			<div class="message">
				{{ session('msg') ?? $errors->first() }}
			</div>
			<div class="register">
				<form class="material-form h-center v-center" action="{{ route('AuthRegister') }}" method="POST" enctype="multipart/form-data">
					@csrf

					<div class="title">Registreren</div>
					
					<div>
						<div class="group fillin">
							<input type="email" name="email" id="reg-user-mail" placeholder=" " required>
							<label>Landstede mail</label>
						</div>
					</div>

					<div>
						<div class="group fillin">
							<input type="password" name="password" id="reg-user-pass" placeholder=" " required>
							<label>Wachtwoord</label>
						</div>
					</div>

					<div>
						<div class="group fillin">
							<input type="password" name="repeat-password" id="reg-user-pass-repeat" placeholder=" " required>
							<label>Wachtwoord herhalen</label>
						</div>
					</div>

					<div class="group bottom">
						<input class="material-shadow" type="submit" name="reg-user-submit" id="reg-user-submit" value="Registreren">
					</div>
				</form>
			</div>

			<div class="login">
				<form class="material-form h-center v-center" action="{{ route('AuthLogin') }}" method="POST" enctype="multipart/form-data">
					@csrf

					<div class="title">Inloggen</div>

					<div>
						<div class="group fillin">
							<input type="email" name="email" id="log-user-mail" placeholder=" " required>
							<label>Landstede mail</label>
						</div>
					</div>
					
					<div>
						<div class="group fillin">
							<input type="password" name="password" id="log-user-pass" placeholder=" " required>
							<label>Wachtwoord</label>
						</div>
					</div>

					<div class="group bottom">
						<input class="material-shadow" type="submit" name="log-user-submit" id="log-user-submit" value="Inloggen">
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection