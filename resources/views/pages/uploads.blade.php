@extends('layouts.page')

@section('content')
	<div class="container-grid full-content">
		<div class="dash-uploads dash-entry padded table-container-vert material-shadow">
			@csrf

			<table class="table-scroll">
				<thead>
					<tr>
						<th class="filter-head" filter-type="alpha" group="file-name"><label>Bestandsnaam</label><label class="filter-button">-</label></th>
						@if (isset($rank) && $rank == 'teacher')
							<th class="filter-head" filter-type="alpha" group="file-user"><label>Gebruiker</label><label class="filter-button">-</label></th>
						@endif
						<th class="filter-head" filter-type="alpha" group="file-date"><label>Upload Datum</label><label class="filter-button">-</label></th>
						<th class="filter-head" filter-type="alpha" group="file-status"><label>Status</label><label class="filter-button">-</label></th>
						<th class="filter-head" filter-type="alpha" group="file-comment"><label>Toelichting</label><label class="filter-button">-</label></th>
						<th class="filter-head" filter-type="alpha" group="file-queue-pos"><label>Wachtrij positie</label><label class="filter-button">-</label></th>
						<th class="save-row"></th>
						<th class="delete-row"></th>
					</tr>
				</thead>
				<tbody class="filter-body">
					@php
						$statusTranslations = [
							'pending' => 'Wachten op goedkeuring..',
							'denied' => 'Afgekeurd',
							'approved' => 'Goedgekeurd'
						];
					@endphp
					@if (isset($uploads) && count($uploads) > 0)
						@foreach ($uploads as $upload)
							<tr class="td-entries" col-id="{{ $upload->id }}">
								<td group="file-name">{{ $upload->real_name }}</td>
								@if (isset($rank) && $rank == 'teacher')
									<td group="file-user">{{ $upload->uploader }}</td>
								@endif
								<td group="file-date">{{ $upload->date_added }}</td>
								<td class="no-ellipsis {{ $rank == 'teacher' ? 'input-container' : '' }}" group="file-status">
									@if (isset($rank) && $rank == 'teacher')
										@if (isset($statuses) && count($statuses) > 0)
											<select class="status-select">
												@foreach ($statuses as $status)
													<option value="{{ $status }}" {{ $upload->status == $status ? 'selected' : ''}}>{{ $statusTranslations[$status] }}</option>
												@endforeach
											</select>
										@else
											<label style="font-style: italic">Geen status gevonden..</label>
										@endif
									@else
										{{ $statusTranslations[$upload->status] }}
									@endif
								</td>
								<td class="no-ellipsis {{ $rank == 'teacher' ? 'input-container' : '' }}" group="file-comment">
									@php
										$comment 			= $upload->comment ? $upload->comment : 'Geen toelichting gegeven';
										$valueContent 		= $upload->comment ? $comment : '';
										$placeholderContent = $upload->comment ? '' : $comment;
									@endphp

									@if (isset($rank) && $rank == 'teacher')
										<input type="text" class="comment-input" value="{{ $valueContent }}" placeholder="{{ $placeholderContent }}">
									@else
										{{ $comment }}
									@endif
								</td>
								<td class="no-ellipsis {{ $rank == 'teacher' ? 'input-container' : '' }}" group="file-queue-pos">
									@php
										$queuePos 			= $upload->queue_pos ? $upload->queue_pos : 'Niet in de wachtrij';
										$valueContent 		= $upload->queue_pos ? $queuePos : '';
										$placeholderContent = $upload->queue_pos ? '' : $queuePos;
									@endphp

									@if (isset($rank) && $rank == 'teacher')
										<input type="number" class="queue-pos-input" value="{{ $valueContent }}" placeholder="{{ $placeholderContent }}" style="text-align: center;">
									@else
										{{ $queuePos }}
									@endif
								</td>
								<td class="save-button smaller-col" route-type="updateUpload">Opslaan</td>
								<td class="delete-button smaller-col" route-type="deleteUpload">X</td>
							</tr>
						@endforeach
					@else
						<tr class="td-entries">
							<td style="text-align: center">Geen uploads gevonden :(</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>

		<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/tables.js') }}"></script>
		@if (isset($rank) && $rank == 'teacher')
			<script type="text/javascript" src="{{ asset('js/overview-edit.js') }}"></script>
		@endif
	</div>
@endsection