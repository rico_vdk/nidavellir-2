@extends('layouts.page')

@section('content')
<div class="container-grid full-content">
		<div class="dash-upload dash-entry padded material-shadow">
			<form class="material-form h-center v-center" action="{{ route('UploadFile') }}" method="post" enctype="multipart/form-data">
				@csrf

				<div class="group file-select">
					<input type="file" name="file" class="file" id="file">
					<label class="button material-shadow{{ $errors->has('file') ? ' danger' : (session('success') ? ' good' : '') }}" for="file">+</label>
					<span class="upload-hint">
						@php
							$hint = $errors->has('file') ? $errors->first('file') : 'Selecteer een bestand';
						@endphp

						{{ session('msg') ?? $hint }}
					</span>
				</div>

				<div class="group bottom">
					<input class="material-shadow" type="submit" name="file-upload-submit" id="file-upload-submit" value="Uploaden">
				</div>
			</form>
		</div>
	</div>
@endsection