@extends('layouts.page')

@section('content')
	<div class="container-grid full-content">
		<div class="dash-users dash-entry padded table-container-vert material-shadow">
			@csrf

			<table id="table-users" class="table-scroll">
				<thead>
					<tr class="th-filters">
						<th class="filter-head" filter-type="alpha" group="user-mail"><label>Gebruiker</label><label class="filter-button">-</label></th>
						<th class="filter-head" filter-type="alpha" group="user-date"><label>Aanmaak Datum</label><label class="filter-button">-</label></th>
						<th class="filter-head" filter-type="alpha" group="user-status"><label>Status</label><label class="filter-button">-</label></th>
						<th class="filter-head" filter-type="alpha" group="user-rank"><label>Rank</label><label class="filter-button">-</label></th>
						<th class="save-row"></th>
						<th class="delete-row"></th>
					</tr>
				</thead>
				<tbody class="filter-body">
					@php
						$statusTranslations = [
							'pending' => 'Wachten op goedkeuring',
							'denied' => 'Afgekeurd',
							'approved' => 'Goedgekeurd'
						];

						$rankTranslations = [
							'student' => 'Student',
							'teacher' => 'Docent'
						];
					@endphp
					@if (isset($users) && count($users) > 0)
						@foreach ($users as $user)
							<tr class="td-entries" col-id={{ $user->id }}>
								<td group="user-mail">{{ $user->email }}</td>
								<td group="user-date">{{ $user->created_at }}</td>
								<td group="user-status" class="no-ellipsis input-container">
									@if (isset($statuses) && count($statuses) > 0)
										<select class="status-select">
											@foreach ($statuses as $status)
												<option value="{{ $status }}" {{ $user->status == $status ? 'selected' : ''}}>{{ $statusTranslations[$status] }}</option>
											@endforeach
										</select>
									@else
										<label style="font-style: italic">Geen status gevonden..</label>
									@endif
								</td>
								<td group="user-rank" class="no-ellipsis input-container">
									@if (isset($ranks) && count($ranks) > 0)
										<select class="rank-select">
											@foreach ($ranks as $rank)
												<option value="{{ $rank }}" {{ $user->rank == $rank ? 'selected' : ''}}>{{ $rankTranslations[$rank] }}</option>
											@endforeach
										</select>
									@else
										<label style="font-style: italic">Geen rank gevonden..</label>
									@endif
								</td>
								<td class="save-button smaller-col" route-type="updateUser">Opslaan</td>
								<td class="delete-button smaller-col" route-type="deleteUser">X</td>
							</tr>
						@endforeach
					@else
						<tr class="td-entries">
							<td style="text-align: center">Geen gebruikers gevonden :(</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
		
		<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/tables.js') }}"></script>
		@if (isset($rank) && $rank == 'teacher')
			<script type="text/javascript" src="{{ asset('js/overview-edit.js') }}"></script>
		@endif
	</div>
@endsection