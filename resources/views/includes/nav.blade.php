<div class="nav-inner">
	<div id="nav-toggle" class="nav-mob-toggle">
		<i class="fas fa-angle-double-right material-shadow"></i>
	</div>

	<div class="nav-primary-list">
		@php
			$page 		= request()->segment(1);
			$rank 		= isset($rank) ? $rank : '';
			$rankLength = strlen($rank);
		@endphp

		<a href="/" class="{{ $page == '' ? 'active' : '' }}">
			<label class="nav-entry-text">
				<i class="fas fa-bread-slice"></i>
				<br>
				<span id="home-tab">Oven</span>
			</label>
		</a>
		<a href="/uploads" class="{{ $page == 'uploads' ? 'active' : '' }}">
			<label class="nav-entry-text">
				<i class="fas fa-archive"></i>
				<br>
				Uploads
			</label>
		</a>
		@if ($rank == 'teacher')
			<a href="/users" class="{{ $page == 'users' ? 'active' : '' }}">
				<label class="nav-entry-text">
					<i class="fas fa-users"></i>
					<br>
					Gebruikers
				</label>
			</a>
		@endif
		<a href="/upload" class="{{ $page == 'upload' ? 'active' : '' }}">
			<label class="nav-entry-text">
				<i class="fas fa-plus"></i>
				<br>
				Uploaden
			</label>
		</a>
		<a href="{{ $rankLength > 0 ? '/logout' : '/auth' }}" class="{{ $rankLength > 0 ? 'logout' : 'login' }}">
			<label class="nav-entry-text">
				<i class="fas {{ $rankLength > 0 ? 'fa-sign-out-alt' : 'fa-sign-in-alt' }}"></i>
				<br>
				{{ $rankLength > 0 ? 'Uitloggen' : 'Inloggen' }}
			</label>
		</a>
	</div>

	<div class="nav-copyright">
		<div>&#169; Landstede<br>{{ ((int) date('Y')) > 2020 ? '2020 - '.date('Y') : '2020' }}</div>
	</div>
</div>