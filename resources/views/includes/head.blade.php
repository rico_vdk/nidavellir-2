<!-- Meta & Out-of-page content. -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=0.95, maximum-scale=0.95, minimum-scale=0.95">
<meta name="copyright" content="Landstede Harderwijk">
<meta name="title" content="Nidavellir">
<meta name="description" content="">
<meta name="designer" content="Rico van der Klein">
<title>Nidavellir</title>

<!-- Styles. -->
<link rel="stylesheet" href="{{ asset('css/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/fa.min.css') }}">

<!-- Site-wide scripts. -->
<!-- <script type="text/javascript" src="#"></script> -->