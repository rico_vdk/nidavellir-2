<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\UploadsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\PrinterDiagnosticsController;
use App\Http\Controllers\PrinterJobController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'Show']);

Route::get('/uploads', [UploadsController::class, 'Show']);
Route::delete('/uploads', [UploadsController::class, 'Delete'])->name('DeleteUpload');
Route::post('/uploads', [UploadsController::class, 'Update'])->name('UpdateUpload');
Route::put('/uploads/update', [AjaxController::class, 'UpdateUpload']);
Route::put('/uploads/delete', [AjaxController::class, 'DeleteUpload']);

Route::get('/upload', [FileUploadController::class, 'Show']);
Route::post('/upload', [FileUploadController::class, 'Upload'])->name('UploadFile');

Route::get('/auth', [AuthController::class, 'Show'])->name('AuthShow');
Route::get('/logout', [AuthController::class, 'Logout'])->name('AuthLogout');
Route::post('/login', [AuthController::class, 'Login'])->name('AuthLogin');
Route::post('/register', [AuthController::class, 'Register'])->name('AuthRegister');

Route::get('/users', [AuthController::class, 'ShowUsers'])->name('AuthUsers');
Route::put('/users/update', [AjaxController::class, 'UpdateUser']);
Route::put('/users/delete', [AjaxController::class, 'DeleteUser']);

Route::get('/api/printer/diagnostics/temperature_flow/{samples}', [PrinterDiagnosticsController::class, 'GetTemperatureFlow']);

Route::get('/api/print_job', [PrinterJobController::class, 'GetJob']);
Route::get('/api/gcode', [PrinterJobController::class, 'GCode']);

Route::get('/queue', [AjaxController::class, 'GetAjaxQueueInfo']);
