<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AllowedDomains implements Rule
{
	protected $allowed = [
		'student.landstede.nl',
		'landstede.nl'
	];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $domain = substr(strrchr($value, "@"), 1);

		return in_array($domain, $this->allowed);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Alleen je Landstede mail is toegestaan voor het registreren!';
    }
}
