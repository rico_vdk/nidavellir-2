<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

	protected $fillable = [
		'path', 'user_id',
		'status', 'comment',
		'queue_pos', 'date_started',
		'date_cleaned', 'date_finished',
	];

	public $timestamps = false;
}
