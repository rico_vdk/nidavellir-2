<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrinterJobController extends Controller
{
	/**
	* Called by the client. Updates either a user or file row based on the request values.
	*
	* @param Request $request
	* @return String
	*/
    public function GetJob()
    {
        $response = PrinterRequestController::Request('GET', 'print_job');

        return response($response->getBody())->header('Content-Type', 'application/json');
    }


/*     public function GCode()
    {
        $response = PrinterRequestController::Request('GET', 'print_job/gcode', true);

        return response($response->getBody())->header('Content-Type', 'application/json');
    } */
}
