<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class PrinterRequestController extends Controller
{
	/**
	* Used to make a request url valid to be accepted by the 3D printer's API.
	*
	* @param String $method
	* @param String $url
	* @param Bool $digest
	* @param Array $options
	* @return Object
	*/
    public static function Request($method, $url, $digest = false, $options = [])
    {
        $client = new Client(['base_uri' => parent::printerApiAdress]);

        if ($digest)
		{
            $options = [
                'auth' => [parent::printerApiId, parent::printerApiKey, 'digest'],
            ];
        }

        $response = $client->request($method, $url, $options);

        return $response;
    }
}
