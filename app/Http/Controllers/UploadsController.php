<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Redirector;

class UploadsController extends Controller
{
	/**
	* Shows an overview of file uploads depending on the user's rank.
	*
	* @return String
	*/
    public function Show()
	{
		if (Auth::check())
		{
			$rank = parent::GetUserRank();
			$uploads;

			if ($rank == 'teacher')
				$uploads = DB::table('files')->select('id', 'user_id', 'real_name', 'date_added', 'comment', 'status', 'queue_pos')->get();
			else
				$uploads = DB::table('files')->select('id', 'user_id', 'real_name', 'date_added', 'comment', 'status', 'queue_pos')->where('user_id', Auth::user()->id)->get();

			
			foreach ($uploads as $upload)
			{
				if ($upload->user_id)
				{
					$upload->{'uploader'} = DB::table('users')->select('email')->where('id', $upload->user_id)->get()[0]->email;
				}
				else
					$upload->{'uploader'} = '-';
			}

			return view('pages.uploads', ['uploads' => $uploads, 'statuses' => ['pending', 'approved', 'denied'], 'rank' => $rank]);
		}
		
		return redirect('auth')->with('msg', 'Je moet aangemeld zijn om uploads te kunnen bekijken!');
	}
}
