<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File as FileHandler;
use App\Models\File as FileModel;
use App\Http\Controllers\AuthController;

class FileUploadController extends Controller
{
	/**
	* Shows the page for uploading a new file.
	*
	* @return String
	*/
    public function Show()
	{
		if (Auth::check())
		{
			return view('pages.new-upload', ['rank' => parent::GetUserRank()]);
		}
		else
			return redirect('auth')->with('msg', 'Je moet aangemeld zijn om te kunnen uploaden!');
	}

	/**
	* Uploads a new file to the server and saves it's info including it's path to the database.
	*
	* @param Request $request
	* @return String
	*/
	public function Upload(Request $request)
	{
		$fileRequest 	= $request->file();
		$fileModel 		= new FileModel;

		if (Auth::check())
		{
			$status = parent::GetUserStatus();

			switch ($status)
			{
				case 'pending':
					return back()->with('msg', 'Je account moet goedgekeurd worden door de docent voordat je mag uploaden.');
				break;

				case 'denied':
					return back()->with('msg', 'Account is afgekeurd. Vraag aan je docent voor de reden van afkeuring of maak een nieuwe aan.');
				break;
			}

			$validate = $request->validate(
				[
					'file' => 'required|max:5240'
				],
				[
					'file.required' => 'Er is geen bestand geselecteerd',
					'file.max'		=> 'Bestandsgrote mag niet groter zijn dan 5mb',
					/* 'file.mimes' => 'Alleen .stl bestanden zijn toegestaan' */ // Reads file contents and will fail 99% of the time. Thanks, Laravel.
				]
			);

			if ($fileRequest)
			{
				$fileOriginalName 	= $request->file->getClientOriginalName();
				$fileName 			= time().'_'.$request->file->getClientOriginalName();

				if (strlen($fileName) > 250)
					return back()->with('msg', 'Bestandsnaam mag niet langer zijn dan 250 karakters!');

				// Custom file extension checking because Laravel acts like an overprotective parent.
				$isStl = strrpos(strtolower($fileOriginalName), '.stl');

				if ($isStl)
				{
					$filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');

					$fileModel->name 		= $fileName;
					$fileModel->real_name	= $fileOriginalName;
					$fileModel->path 		= '\\app\\public\\' . $filePath;
					$fileModel->user_id 	= $this->GetUserId();
					$fileModel->status 		= 'pending';
					$fileModel->comment 	= '';

					$fileModel->save();

					return back()->with('msg', 'Bestand is geupload');
				}
				else
					return back()->with('msg', 'Bestand moet van type .stl zijn!');
			}
		}
		else
			return back()->with('msg', 'Je moet hiervoor aangemeld zijn!');
	}
}
