<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	const printerApiAdress 	= 'http://10.0.1.42/api/v1/';
	const printerApiId 		= 'df9e131b408f608bba8a9637a0bfc867';
    const printerApiKey 	= '00e9a21849f893086b073805eb03b775bc28815e69fb8b428830477d4bfaf66e';

	/**
	* Returns the currently active user's rank.
	*
	* @return String
	*/	
	public static function GetUserRank()
	{
		return Auth::check() ? Auth::user()->rank : '';
	}

	/**
	* Returns the currently active user's status.
	*
	* @return String
	*/
	public static function GetUserStatus()
	{
		return Auth::check() ? Auth::user()->status : '';
	}

	/**
	* Returns the currently active user's id.
	*
	* @return String
	*/
	public static function GetUserId()
	{
		return Auth::check() ? Auth::user()->id : '';
	}
}
