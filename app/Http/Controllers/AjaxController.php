<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File as FileHandler;
use App\Models\File as FileModel;
use App\Models\User;

class AjaxController extends Controller
{
	private $allowedValues 	= [
		'user' 		=> [
			'status' 	=> ['pending', 'approved', 'denied'],
			'rank' 		=> ['student', 'teacher'],
			'delete'
		],
		'upload' 	=> [
			'status' 	=> ['pending', 'approved', 'denied'],
			'comment',
			'queue-pos',
			'delete'
		]
	];

	/**
	* Called by the client.
	* Used to retrieve a list of files in the current queue, as well as their position.
	* 
	* @return String
	*/
	public function GetAjaxQueueInfo()
	{
		$queue = DB::table('files')->select('id', 'name', 'real_name', 'queue_pos')->whereNotNull('queue_pos')->orderBy('queue_pos')->get();

		return response()->json(['queue' => $queue]);
	}

	/**
	 * Called by the client.
	 * Updates an upload based on the given row.
	 * 
	 * @return String
	 */
	public function UpdateUpload(Request $request)
	{
		$currentUserRank 	= parent::GetUserRank();
		$currentUserId 		= parent::GetUserId();
		$response 			= ['success' => false];

		if ($currentUserRank == 'teacher')
		{
			$fileId 		= $request->input('id');
			$routeType 		= $request->input('routeType');
			$newStatus 		= $request->input('status');
			$newComment 	= $request->input('comment');
			$newQueuePos 	= $request->input('queuePos');
			$fileModel 		= FileModel::find($fileId);
			$oldStatus 		= $fileModel->status;
			$oldComment 	= $fileModel->comment;
			$oldQueuePos	= $fileModel->queue_pos;

			$response['routeType'] 	= $routeType;
			$response['success']	= true;

			if ($newStatus != $oldStatus && $this->ValueAllowed($newStatus, $this->allowedValues['upload']['status']))
				$fileModel->status = $newStatus;

			if ($newComment != $oldComment)
				$fileModel->comment = $newComment; // https://stackoverflow.com/questions/51126162/laravel-eloquent-is-sql-injection-prevention-done-automatically
			
			$fileModel->save(); // Because queues are complicated.
			
			if ($newQueuePos != $oldQueuePos)
			{
				if ($newQueuePos != '0' && $newQueuePos != '' && $newQueuePos > 0) // Adds or updates item in the queue.
				{
					$fileArrayPos 	= $oldQueuePos - 1;
					$targetQueuePos = $newQueuePos;
					$targetArrayPos	= $targetQueuePos - 1;
					$queuedFiles 	= json_decode(json_encode($this->GetQueuedFiles())); // PHP can't properly cast an object to an array.
					$fileObject 	= [(object) [
						'id' => $fileId, 'queue_pos' => (int) $newQueuePos
					]];

					if (array_key_exists($fileArrayPos, $queuedFiles))
						array_splice($queuedFiles, $fileArrayPos, 1);
					
					array_splice($queuedFiles, $targetArrayPos, 0, $fileObject);

					for ($i = 0; $i < count($queuedFiles); $i++)
					{
						$queuedFileModel = FileModel::find($queuedFiles[$i]->id);
						$queuedFileModel->queue_pos = $i + 1;
						$queuedFiles[$i]->queue_pos = $i + 1;

						$queuedFileModel->save();
					}
				}
				elseif ($newQueuePos == '' && $oldQueuePos != null) // Removes item from the queue.
				{
					$queuedFiles = $this->GetQueuedFiles();

					$fileModel->queue_pos = null;
				}
				elseif ($newQueuePos < 1)
					$response['error'] = 'Cannot give file a position below 1.';
			}
		}
		else
			$response['error'] = 'Your rank does not allow you to change upload data.';

		return response()->json($response);
	}

	/**
	 * Called by the client.
	 * Updates a user based on the given row.
	 * 
	 * @return String
	 */
	public function UpdateUser(Request $request)
	{
		$currentUserId 		= parent::GetUserId();
		$currentUserRank 	= parent::GetUserRank();
		$response 			= ['success' => false];

		if ($currentUserRank == 'teacher')
		{
			$userId = $request->input('id');

			if ($currentUserId != $userId)
			{
				$routeType 	= $request->input('routeType');
				$newStatus 	= $request->input('status');
				$newRank 	= $request->input('rank');
				$userModel 	= User::find($userId);
				$oldStatus 	= $userModel->status;
				$oldRank 	= $userModel->rank;

				$response['routeType'] = $routeType;
				
				if ($newStatus != $oldStatus && $this->ValueAllowed($newStatus, $this->allowedValues['user']['status']))
					$userModel->status = $newStatus;
				
				if ($newRank != $oldRank && $this->ValueAllowed($newRank, $this->allowedValues['user']['rank']))
					$userModel->rank = $newRank;

				$userModel->save();
			}
			else
				$response['error'] = 'You cannot change your own data.';
		}
		else
			$response['error'] = 'Your rank does not allow you to change user data.';

		return response()->json($response);
	}

	/**
	 * Called by the client.
	 * Deletes a file from the server and removes it's data from the database.
	 * 
	 * @return String
	 */
	public function DeleteUpload(Request $request)
	{
		$currentUserRank 	= parent::GetUserRank();
		$currentUserId 		= parent::GetUserId();
		$fileId 			= $request->input('id');
		$routeType 			= $request->input('routeType');
		$fileModel 			= FileModel::find($fileId);
		$path 				= storage_path() . $fileModel->path;
		$response 			= ['routeType' => $routeType, 'success' => false];

		if ($currentUserRank == 'teacher' || $fileModel->user_id == $currentUserId)
		{
			if (file_exists($path))
				unlink($path);
			else
				$response['note'] = 'File does not exist on the server, but deleted the database link anyways.';

			DB::table('files')->delete($fileModel->id);

			$response['success'] = true;
		}
		else
			$response['error'] = 'You do not have the required rights to remove this file.';

		return response()->json($response);
	}

	/**
	 * Called by the client.
	 * Removes a user based on the given row, including their files.
	 * 
	 * @return String
	 */
	public function DeleteUser(Request $request)
	{
		$currentUserRank 	= parent::GetUserRank();
		$currentUserId 		= parent::GetUserId();
		$userId 			= $request->input('id');
		$response 			= ['success' => false];

		if ($currentUserRank == 'teacher')
		{
			if ($currentUserId != $userId)
			{
				$userFiles = DB::table('files')->select('id', 'path')->where('user_id', $user->id)->get();

				foreach ($userFiles as $userFile)
				{
					$filePath = storage_path() . $userFile->path;

					if (file_exists($filePath))
						unlink($filePath);
				}

				FileModel::where('user_id', $user->id)->delete();

				$user->delete();

				$response['success'] = true;
			}
			else
				$response['error'] = 'Please do not delete yourself. You have so much to live for!';
		}
		else
			$response['error'] = 'Your rank does not allow you to eradicate other users.';
		
		return response()->json($response);
	}

	/**
	* Used to see whether or not a given value sent by the client can be safely accepted.
	* Accepted values are stored in the {$allowedValues} array.
	*
	* @param Any $val
	* @param Array $array
	* @return Bool
	*/
	private function ValueAllowed($val, $array)
	{
		return in_array($val, $array) || array_key_exists($val, $array);
	}

	/**
	* Returns a list of files currently in the queue.
	* This is used by the class itself, rather than for responding to client requests.
	*
	* @return Array
	*/
	private function GetQueuedFiles()
	{
		return DB::table('files')->select('id', 'queue_pos')->whereNotNull('queue_pos')->orderBy('queue_pos')->get();
	}
}
