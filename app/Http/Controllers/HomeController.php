<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
	/**
	* Shows the index page for Nidavellir.
	*
	* @param Request $request
	* @return String
	*/
    public function Show()
    {
		$queue = DB::table('files')->select('name', 'queue_pos')->whereNotNull('queue_pos')->orderBy('queue_pos')->get();

        return view('pages.home', ['queue' => $queue, 'rank' => parent::GetUserRank()]);
    }
}
