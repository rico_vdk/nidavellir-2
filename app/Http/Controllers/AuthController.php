<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Redirector;
use App\Rules\AllowedDomains;
use App\Models\User;

class AuthController extends Controller
{
	/**
	* Shows the authentication page.
	*
	* @return String
	*/
    public function Show()
	{
		return view('pages.auth', ['rank' => parent::GetUserRank()]);
	}

	/**
	* Shows a list of all the registered users, only when logged in as an admin.
	*
	* @return String
	*/
	public function ShowUsers()
	{
		if (Auth::check())
		{
			if (parent::GetUserRank() == 'teacher')
			{
				$users = DB::table('users')->select('id', 'email', 'created_at', 'status', 'rank')->get();

				// Manually pass the enum values instead of writing a big custom query to dynamically retrieve them.
				// Considering the fact that these likely won't ever be changed, this is more efficient.
				return view('pages.users', [
					'users' 	=> $users,
					'statuses' 	=> ['pending', 'approved', 'denied'],
					'ranks' 	=> ['student', 'teacher']
				]);
			}
		}

		return redirect('auth')->with('msg', 'Je moet docent zijn om gebruikers te kunnen bekijken!');
	}

	/**
	* Validates form data on registration and saves it's information to the database once validated.
	*
	* @return String
	*/
	public function Register()
	{
		$this->validate(
			request(),
			[
				'email' 	=> ['required', 'email', 'string', 'unique:users', new AllowedDomains],
				'password' 	=> ['required_with:repeat-password', 'same:repeat-password']
			],
			[
				'email.required' 			=> 'Je moet wel je Landstede mail opgeven... >:(',
				'email.unique' 				=> 'Het opgegeven email adres is al gekoppeld aan een account.',
				'password.required_with' 	=> 'Zorg dat je alle velden invult!',
				'password.same' 			=> 'Herhaalde wachtwoord is niet hetzelfde als die was opgegeven. Probeer het opnieuw.'
			]
		);

		$user = User::create(request(['email', 'password']));
		$email = request('email');

		auth()->login($user);

		return redirect('auth')->with(
			'msg', 'Je bent nu geregistreerd en ingelogd, '.$email.'!'
		);
	}

	/**
	* Attempts to create a user session given the client's form data.
	*
	* @return String
	*/
	public function Login(Request $request)
	{
		$email = request('email');

		if (!auth()->attempt(request(['email', 'password'])))
		{
            return redirect('auth')->with(
                'msg', 'Inloggen mislukt. Probeer het opnieuw.'
            );
        }
		else
		{
			return redirect('auth')->with(
				'msg', 'Je bent nu ingelogd, '.$email.'!'
			);
		}
	}

	/**
	* Wipes the currently active user session.
	*
	* @param Request $request
	* @return String
	*/
	public function Logout()
	{
		auth()->logout();

		return redirect('auth')->with('msg', 'Je bent nu uitgelogd.');
	}
}
