<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PrinterRequestController;

class PrinterDiagnosticsController extends Controller
{
	/**
	* Called by the client.
	* Used to retrieve the chart data for the 3D printer, by making a request to its API.
	*
	* @param String $samples
	* @return String
	*/
    public function GetTemperatureFlow($samples)
    {
        $response = PrinterRequestController::Request('GET', 'printer/diagnostics/temperature_flow/' . $samples);

        return response($response->getBody())->header('Content-Type', 'application/json');
    }
}
