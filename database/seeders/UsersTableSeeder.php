<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Create a default admin on app setup.
     *
     * @return void
     */
    public function run()
    {
		$adminMail = config('admin.admin_email');
		
        if ($adminMail)
		{
            User::firstOrCreate(
                ['email' 	=> $adminMail],
				[
					'password' => config('admin.admin_password'),
					'status' 	=> 'approved',
					'rank' 	=> 'teacher'
				]
            );
        }
    }
}
