<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id('id')->autoIncrement();

			$table->integer('user_id');
			$table->string('name', 250);
			$table->string('real_name', 250);
			$table->string('path', 500);
			$table->enum('status', ['pending', 'approved', 'denied']);
			$table->string('comment', 50)->nullable();
			$table->integer('queue_pos')->nullable();

            $table->timestamp('date_added');
			$table->timestamp('date_started')->nullable();
			$table->timestamp('date_cleaned')->nullable();
			$table->timestamp('date_finished')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
